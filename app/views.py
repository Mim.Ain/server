from django.http import HttpResponse
from django.shortcuts import render
from app.models import user
import datetime
import requests
from django.views.decorators.csrf import csrf_exempt


@csrf_exempt
def register(request): 
    data = request.POST.dict()
    users = user.objects.all()
    for userr in users :
       if userr.username == data['username'] :
          return HttpResponse('False')

    user.objects.create(username = data['username'], password = data['password'])
    return HttpResponse('True')
          

@csrf_exempt 
def login(request):
    data = request.POST.dict()
    users = user.objects.all()
    for userr in users :
       if userr.username == data['username'] :
         if userr.password == data['password'] :
            request.session['user_id'] = userr.id
            return HttpResponse('You are logged in.')
            
         else :
            return HttpResponse("Your username and password doesn't match.")
    return HttpResponse('incorrect username.')     

 
@csrf_exempt  
def logout(request):
    if 'user_id' in request.session :
        del request.session['user_id']
        return HttpResponse('You are logged out.')
    else :
        return HttpResponse('You are not logged in.')

 
@csrf_exempt
def dummy(request):
   return HttpResponse(datetime.datetime.now())      
